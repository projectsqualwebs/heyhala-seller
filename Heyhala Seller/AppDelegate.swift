//
//  AppDelegate.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import IQKeyboardManager

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
   
   var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared().isEnabled = true
        self.initializeGoogleMap()
        return true
    }
    
    func initializeGoogleMap(){
        GMSPlacesClient.provideAPIKey("AIzaSyBvOO-m6sV_jFWGUWIx5aPqBGfgI8d3HeI")
        GMSServices.provideAPIKey("AIzaSyBvOO-m6sV_jFWGUWIx5aPqBGfgI8d3HeI")

     //   GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
    }

}

