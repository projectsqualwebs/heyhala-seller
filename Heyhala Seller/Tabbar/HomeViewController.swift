//
//  HomeViewController.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//

import UIKit
import Charts
import RAMAnimatedTabBarController
import UICircularProgressRing

class HomeViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet var chartView: BarChartView!
    @IBOutlet weak var homeTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var progressView: UICircularProgressRing!
    @IBOutlet weak var overallSales: DesignableUILabel!
    @IBOutlet weak var orderCompleted: DesignableUILabel!
    @IBOutlet weak var orderReurned: DesignableUILabel!
    @IBOutlet weak var newOrders: DesignableUILabel!
    @IBOutlet weak var orderInProcess: DesignableUILabel!
    
    
    var months =  ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug"]
    let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 8.0, 15.0, 10.0]
    var dataEntries: [BarChartDataEntry] = []
    weak var axisFormatDelegate: IAxisValueFormatter?

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chartView.delegate = self
        axisFormatDelegate = self
        var formatter = UICircularProgressRingFormatter()
        formatter.valueIndicator = ""
        formatter.decimalPlaces = 0
        formatter.rightToLeft = true
        self.progressView.valueFormatter = formatter
        chartView.setVisibleXRangeMaximum(8)
        self.homeTab.animation.iconSelectedColor = .white
        self.homeTab.animation.textSelectedColor = .white
        homeTab.playAnimation()
        self.setChart(dataPoints: months, values: unitsSold)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NavigationController.shared.getDashboardData(view: self) { response in
            self.overallSales.text = "\(response.status.overallSales ?? 0)"
            self.orderCompleted.text = "\(response.status.completedOrders ?? 0)"
            self.orderReurned.text = "\(response.status.returnOrders ?? 0)"
            self.newOrders.text = "\(response.status.newOrders ?? 0)"
            self.orderInProcess.text = "\(response.status.orderInProgress ?? 0)"
        }
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        chartView.noDataText = "You need to provide data for the chart."
        
        // Prevent from setting an empty data set to the chart (crashes)
        guard dataPoints.count > 0 else { return }
        
        var dataEntries = [BarChartDataEntry]()
        
        for i in 0..<dataPoints.count {
            let entry = BarChartDataEntry(x: Double(i), y: values[i], data: months as AnyObject?)
            dataEntries.append(entry)
        }
        self.chartView.layer.cornerRadius = 15
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "")
        chartDataSet.drawValuesEnabled = true
        chartDataSet.valueFont = UIFont.systemFont(ofSize: 15.0)
        chartDataSet.valueTextColor = .white
        chartDataSet.colors = [UIColor.white]
        chartDataSet.colors = [UIColor.white]
        chartView.fitBars = true
        chartDataSet.highlightColor = UIColor.orange.withAlphaComponent(0.3)
        chartDataSet.highlightAlpha = 1
        let chartData = BarChartData(dataSet: chartDataSet)
        let groupSpace = 0.2
        let barSpace = 0.9
        let barWidth = 0.2
        // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"

        let startVal = 0

        chartData.barWidth = barWidth;
        chartData.groupBars(fromX: Double(startVal), groupSpace: groupSpace, barSpace: barSpace)
        chartView.data = chartData
        let xAxisValue = chartView.xAxis
        xAxisValue.valueFormatter = axisFormatDelegate
        
        //Animation bars
        chartView.animate(xAxisDuration: 0.0, yAxisDuration: 1.0, easingOption: .easeInCubic)
        
        
        // X axis configurations
        chartView.xAxis.labelCount = self.months.count
        chartView.xAxis.spaceMin = 0.5
        chartView.xAxis.spaceMax = 0.5
        chartView.xAxis.granularity = 1
        chartView.xAxis.granularityEnabled = true
        chartView.xAxis.drawAxisLineEnabled = false
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.labelFont = UIFont.systemFont(ofSize: 15.0)
        chartView.xAxis.labelTextColor = UIColor.white
        chartView.xAxis.labelPosition = .bottom
        
      
        
        // Right axis configurations
        chartView.rightAxis.drawAxisLineEnabled = false
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawLabelsEnabled = false
        
        chartView.leftAxis.drawLabelsEnabled = false
        chartView.leftAxis.drawAxisLineEnabled = false
        chartView.leftAxis.drawGridLinesEnabled = false
        
        // Other configurations
        chartView.scaleXEnabled = true
        chartView.scaleYEnabled = false
        
   }
    
    
}

extension HomeViewController: IAxisValueFormatter,ChartViewDelegate {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value)]
    }
  
    func groupBars(fromX: Double, groupSpace: Double, barSpace: Double){
        
    }
    
    func drawDataSet(context: CGContext, dataSet: IBarChartDataSet, index: Int){
        
    }
}
