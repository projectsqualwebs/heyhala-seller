//
//  PaymentInfoViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 27/04/21.
//

import UIKit

class PaymentInfoViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var trnasactionTable: UITableView!
    @IBOutlet weak var cardView: View!
    @IBOutlet weak var titleLabel: DesignableUILabel!
    @IBOutlet weak var titleDescription: DesignableUILabel!
    @IBOutlet weak var cardButton: View!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    
    var isEventRequest  = false
    var serviceData = [ServiceRequestResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
   
    override func viewDidAppear(_ animated: Bool) {
        self.initializeView()
    }
    
    func initializeView(){
        if(self.isEventRequest){
            self.cardView.isHidden = true
            self.cardButton.isHidden = true
        }
        
        self.titleLabel.text = isEventRequest ? "Event Request":"Transactions"
        self.titleDescription.text = isEventRequest ? "Requests on event will appear here":"Transactions will appear here"
        if(self.isEventRequest){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SERVICE_REQUEST, method: .get, parameter: nil, objectClass: GetServiceRequest.self, requestCode: U_GET_SERVICE_REQUEST) { (response) in
                self.serviceData = response.response
                self.trnasactionTable.reloadData()
                ActivityIndicator.hide()
            }
        }else {
            
        }
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension PaymentInfoViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollection", for: indexPath) as! CardCollection
        return cell
    }
    
    
}


extension PaymentInfoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.serviceData.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return self.serviceData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell") as! TransactionCell
        let val = self.serviceData[indexPath.row]
        if(self.isEventRequest){
            cell.dateLabel.isHidden = true
            cell.nameLabel.text = val.description
            cell.descriptionLabel.text = self.convertTimestampToDate(val.date ?? 0, to: "dd/MM/yyyy")
            if(val.status?.status == "1"){
                cell.status.text = "Pending ⌛"
            }else if(val.status?.status == "2"){
                cell.status.text = "Accepted by you"
            }else if(val.status?.status == "3"){
                cell.status.text = "Rejected 🔴"
            }else if(val.status?.status == "4"){
                cell.status.text = "Accepted 🟢"
            }
        }else {
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isEventRequest){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderRequestViewController") as! OrderRequestViewController
            myVC.serviceRequestData = self.serviceData[indexPath.row]
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
}

class TransactionCell: UITableViewCell {
    //MARK: IBOutlets
    
    @IBOutlet weak var descriptionLabel: DesignableUILabel!
    @IBOutlet weak var nameLabel: DesignableUILabel!
    @IBOutlet weak var status: DesignableUILabel!
    @IBOutlet weak var dateLabel: DesignableUILabel!
}


class CardCollection: UICollectionViewCell {
    //MARK: IBOutlets
    
}
