//
//  AddProductViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 24/04/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import PINRemoteImage

protocol NewEntry {
    func newEntry()
}

class AddProductViewController: UIViewController, SelectFromPicker {
    func selectedItem(name: String, index: Int) {
        self.selectedCategory = self.categoryData[index]
        self.category.text = name
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var editLabel: DesignableUILabel!
    @IBOutlet weak var categoryView: View!
    @IBOutlet weak var descriptionLabel: DesignableUILabel!
    @IBOutlet weak var logoImage: ImageView!
    @IBOutlet weak var saveButtonLabel: DesignableUILabel!
    @IBOutlet weak var licenseImage: ImageView!
    @IBOutlet weak var productName: DesignableUITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var category: DesignableUILabel!
    @IBOutlet weak var location: DesignableUILabel!
    @IBOutlet weak var priceView: UIStackView!
    @IBOutlet weak var priceField: DesignableUITextField!
    @IBOutlet weak var licenseView: View!
    @IBOutlet weak var keywordField: DesignableUITextField!
    @IBOutlet weak var ingredients: DesignableUITextField!
    @IBOutlet weak var locationView: View!
    @IBOutlet weak var preperationField: DesignableUITextField!
    @IBOutlet weak var preperationView: UIView!
    @IBOutlet weak var preperationSwitch: UISwitch!
    
    
    var newEntryDelegate: NewEntry? = nil
    var screenType = 1
    var categoryData = [GetCategoryResponse]()
    var imagePath  = ""
    var imagePath2  = ""
    var imageName = ""
    var currentPick = 1
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    let picker = UIImagePickerController()
    var selectedCategory = GetCategoryResponse()
    var navigationFromVerification = false
    var editProduct = GetStoreResponse()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        self.textView.delegate = self
        NavigationController.shared.getAllCategories { (response) in
            self.categoryData = response
        }
        priceField.attributedPlaceholder = NSAttributedString(string: "Price",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        keywordField.attributedPlaceholder = NSAttributedString(string: "Keywords",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        ingredients.attributedPlaceholder = NSAttributedString(string: "Ingredients",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        productName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        preperationField.attributedPlaceholder = NSAttributedString(string: "Enter preperation time in minutes",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        if(screenType == 2){
            self.editLabel.text = "Edit Product"
            self.productName.text = editProduct.name
            self.priceField.text = "\(editProduct.price ?? 0)"
            self.textView.text = editProduct.description
            self.logoImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (editProduct.image ?? "")))
            self.licenseImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (editProduct.license ?? "")))
            self.location.text = editProduct.location ?? ""
            self.imagePath = editProduct.image   ?? ""
            self.imagePath2 = editProduct.license ?? ""
            self.location.text = editProduct.location ?? ""
            self.preperationField.text = "\(editProduct.preprationTime ?? 0)"
            self.preperationSwitch.isOn = editProduct.preperationStatus == 0 ? true:false
            self.preperationField.isHidden = editProduct.preperationStatus == 0 ? true:false
            self.ingredients.text = editProduct.ingredients ?? ""
            self.keywordField.text = editProduct.keywords ?? ""
            self.latitude = CLLocationDegrees(editProduct.latitude ?? "0")!
            self.longitude = CLLocationDegrees(editProduct.longitude ?? "0")!
            self.categoryView.isHidden = false
            self.preperationView.isHidden = false
            
            let category = self.categoryData.filter{
                $0.id == editProduct.category
            }
            if(category.count > 0){
                self.category.text = category[0].category_name
                self.selectedCategory = category[0]
            }
        }else if(screenType == 3){
            self.licenseView.isHidden = false
            self.priceView.isHidden = true
            self.locationView.isHidden = false
            self.editLabel.text = "Add Store"
            self.descriptionLabel.text = "Add your store details"
            self.categoryView.isHidden = true
            self.preperationView.isHidden = true
            self.saveButtonLabel.text = "Save Store Details"
            NavigationController.shared.getAllStores { (response) in
                if(response.count > 0){
                    self.productName.text = response[0].name
                    self.textView.text = response[0].description
                    self.logoImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (response[0].logo ?? "")))
                    self.licenseImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (response[0].license ?? "")))
                    self.location.text = response[0].location
                    self.imagePath = response[0].logo ?? ""
                    self.imagePath2 = response[0].license ?? ""
                    self.latitude = CLLocationDegrees(response[0].latitude ?? "0")!
                    self.longitude = CLLocationDegrees(response[0].longitude ?? "0")!
                    
                    let category = self.categoryData.filter{
                        $0.id == response[0].category
                    }
                    if(category.count > 0){
                        self.selectedCategory = category[0]
                    }
                }
            }
        }else {
            self.editLabel.text = "Add Product"
        }
        nameLabel.attributedPlaceholder = NSAttributedString(string: "Name",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    
    //MARK: IBActions
    @IBAction func preperationAction(_ sender: UISwitch) {
        if(sender.isOn){
            self.preperationField.isHidden = false
        }else {
            self.preperationField.isHidden = true
        }
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addLogoAction(_ sender: UIButton) {
        self.currentPick = sender.tag
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.addActionSheetForiPad(actionSheet: alert)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func categoryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        for val in categoryData {
            if(val.category_name != "EVENTS"){
                myVC.pickerData.append(val.category_name ?? "")
            }
        }
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func locationAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                    UInt(GMSPlaceField.placeID.rawValue) |
                                                    UInt(GMSPlaceField.coordinate.rawValue) |
                                                    GMSPlaceField.addressComponents.rawValue |
                                                    GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func addProductAction(_ sender: Any) {
        if(self.imagePath.isEmpty){
            self.showMsg(controller: self, text: "Pick Logo image")
        }else if(self.nameLabel.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter name")
        }else if(self.screenType != 3 && self.priceField.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter price")
        }else if(self.screenType != 3 && selectedCategory.id == nil){
            self.showMsg(controller: self, text: "Select category")
        }else if(self.textView.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter description")
        }else if(((self.location.text!.isEmpty || (self.location.text == "Address"))) && screenType == 3){
            self.showMsg(controller: self, text: "Enter address")
        }else if(self.imagePath2.isEmpty && self.screenType == 3){
            self.showMsg(controller: self, text: "Pick License image")
        }else {
            var url = String()
            var param = [String:Any]()
            if(self.screenType == 3){
                if(Singleton.shared.storeData.count > 0){
                    url = U_BASE + U_BUSINESS_UPDATE + "\(Singleton.shared.storeData[0].id ?? "")"
                }else {
                    url = U_BASE + U_CREATE_BUSINESS
                }
                param = [
                    "name": self.productName.text,
                    "description": self.textView.text,
                    "location": self.location.text,
                    "logo": self.imagePath,
                    "license":self.imagePath2,
                    "latitude": self.latitude,
                    "longitude": self.longitude
                ]
            }else if(self.screenType == 1){
                url = U_BASE + U_ADD_PRODUCT
                param = [
                    "business": Singleton.shared.storeData.count > 0 ? Singleton.shared.storeData[0].id ?? "":"",
                    "name": self.productName.text ?? "",
                    "description": self.textView.text ?? "",
                    "price": self.priceField.text ?? "",
                    "category": self.selectedCategory.id ?? "",
                    "preprationTime": self.preperationField.text ?? "",
                    "image":self.imagePath,
                    "keywords":self.keywordField.text ?? "",
                    "ingredients":self.ingredients.text ?? "",
                    "preperationStatus":self.preperationField.isHidden ? 0:1
                ]
            }else if(self.screenType == 2){
                url = U_BASE + U_UPDATE_PRODUCT + "\(self.editProduct.id ?? "")"
                param = [
                    "business": Singleton.shared.storeData.count > 0 ? Singleton.shared.storeData[0].id ?? "":"",
                    "name": self.productName.text ?? "",
                    "description": self.textView.text ?? "",
                    "price": self.priceField.text ?? "",
                    "preprationTime": 65,
                    "image":self.imagePath,
                    "keywords":self.keywordField.text ?? "",
                    "ingredients":self.ingredients.text ?? "",
                    "category": self.selectedCategory.id ?? "",
                    "preperationStatus":self.preperationField.isHidden ? 0:1
                ]
            }
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: url) { (response) in
                ActivityIndicator.hide()
                if(self.screenType == 3){
                    Singleton.shared.storeData = []
                    NavigationController.shared.getAllStores { (response) in
                        
                    }
                    self.showMsg(controller: self, text: "Store saved successfully")
                    
                    if(self.navigationFromVerification){
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                        self.navigationController?.pushViewController(myVC, animated: true)
                        return
                    }
                    self.newEntryDelegate?.newEntry()
                }else if(self.screenType == 1){
                    self.showMsg(controller: self, text: "Product saved successfully")
                    self.newEntryDelegate?.newEntry()
                }else if(self.screenType == 2){
                    self.showMsg(controller: self, text: "Product updated successfully")
                    self.newEntryDelegate?.newEntry()
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}


extension AddProductViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "image": data!,
                "type": self.screenType == 3 ? 3:4
            ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, objectClass: UploadImage.self, fileName: imageName) { (response) in
                
                if(self.currentPick == 1){
                    self.logoImage.image = cropImage
                    self.imagePath = response.response?.url ?? ""
                }else {
                    self.imagePath2 = response.response?.url ?? ""
                    self.licenseImage.image = cropImage
                }
                
                ActivityIndicator.hide()
                
            }
            
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
}



extension AddProductViewController: GMSAutocompleteViewControllerDelegate, UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Description"){
            self.textView.text = ""
        }
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.location.text = place.formattedAddress
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
