//
//  OrdersViewController.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//

import UIKit
import RAMAnimatedTabBarController

class OrdersViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var orderTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var sortybyCollection: UICollectionView!
    @IBOutlet weak var orderTable: ContentSizedTableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    
    let data = ["All", "New", "In Progress", "Completed"]
    var selectedType = 1
    var orderData = [OrderResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderTab.animation.iconSelectedColor = .white
        self.orderTab.animation.textSelectedColor = .white
        orderTab.playAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.orderData.count ==  0 && Singleton.shared.storeData.count > 0){
          self.getOrderData()
        }
    }
    
    func getOrderData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ORDERS, method: .post, parameter: ["business":Singleton.shared.storeData[0].id ?? "str-sktwief3kpxzewgs", "type":self.selectedType], objectClass: GetOrders.self, requestCode: U_GET_ORDERS) { (response) in
            self.orderData = response.response
            self.orderTable.reloadData()
            ActivityIndicator.hide()
        }
        
    }
    
    //MARK: IBActions
    
}

extension OrdersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollection", for: indexPath) as! ImageCollection
        cell.itemName.text = data[indexPath.row]
        if(selectedType == indexPath.row+1){
            cell.itemView.backgroundColor = .white
            cell.itemName.textColor = primaryColor
        }else {
            cell.itemView.backgroundColor = grayColor
            cell.itemName.textColor = .white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedType = indexPath.row + 1
        self.sortybyCollection.reloadData()
        self.getOrderData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
                    label.text = data[indexPath.row]
                    label.sizeToFit()
        return CGSize(width: label.frame.width + 40, height: 50)
    }
}

extension OrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.orderData.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return self.orderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableCell") as! ProductTableCell
        let val = self.orderData[indexPath.row]
        cell.productName.text = val.orderItems[0]
            .business?.name
        cell.productPrice.text = "Order Id - \(val.order_id ?? 0)"
        cell.date.text = String((val.created_at ?? "").prefix(10))
        switch val.order_status {
        case "0":
            cell.orderStatus.text = "Pending"
            break
        case "1":
            cell.orderStatus.text = "Accepted"
            break
        case "2":
            cell.orderStatus.text = "Rejected"
            break
        case "3":
            cell.orderStatus.text = "Preparing"
            break
        case "4":
            cell.orderStatus.text = "Out for delivery"
            break
        case "5":
            cell.orderStatus.text = "Delivered"
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
          myVC.orderDetail = self.orderData[indexPath.row]
          self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}
