//
//  OrderRequestViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 28/04/21.
//

import UIKit

class OrderRequestViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var startField: UITextField!
    @IBOutlet weak var endField: UITextField!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var userComment: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    var serviceRequestData = ServiceRequestResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateField.attributedPlaceholder = NSAttributedString(string: "Date",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        amountField.attributedPlaceholder = NSAttributedString(string: "Enter Amount",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        startField.attributedPlaceholder = NSAttributedString(string: "Start Time",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        locationField.attributedPlaceholder = NSAttributedString(string: "Location",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        endField.attributedPlaceholder = NSAttributedString(string: "End Time",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.initializeView()
    }
    
    func initializeView(){
        self.dateField.text = self.convertTimestampToDate(self.serviceRequestData.date ?? 0, to: "yyyy-MM-dd")
        self.startField.text = self.convertTimestampToDate(self.serviceRequestData.startTime ?? 0, to: "h:mm a")
        self.endField.text = self.convertTimestampToDate(self.serviceRequestData.endTime ?? 0, to: "h:mm a")
        self.amountField.text = "SAR \(self.serviceRequestData.status?.price ?? 0)"
        self.locationField.text =  self.serviceRequestData.location
        self.textView.text = self.serviceRequestData.description
        if(self.serviceRequestData.status?.status == "3"){
            self.submitButton.setTitle("Accepted by customer", for: .normal)
        }else if(self.serviceRequestData.status?.status == "4"){
            self.submitButton.setTitle("Rejected", for: .normal)
        }else if(self.serviceRequestData.status?.status == "1"){
            self.submitButton.setTitle("Submit", for: .normal)
        }else {
            self.submitButton.setTitle("Request sent", for: .normal)
            self.submitButton.isUserInteractionEnabled = false
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        if(sender.titleLabel?.text == "Submit"){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_SERVICE_REQUEST + "\(self.serviceRequestData.id ?? "")", method: .post, parameter: ["price":self.amountField.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_UPDATE_SERVICE_REQUEST) { response in
                self.navigationController?.popViewController(animated: true)
                self.showMsg(controller: self, text: "Request send successfully")
            }
        }else if(sender.titleLabel?.text == "Accepted by customer"){
            
        }else if(sender.titleLabel?.text == "Request sent"){
            
        }else if(sender.titleLabel?.text == "Rejected"){
            
        }
    }
    
}
