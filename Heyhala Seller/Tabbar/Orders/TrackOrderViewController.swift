//
//  TrackOrderViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 28/04/21.
//

import UIKit

class TrackOrderViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var orderId: DesignableUILabel!
    @IBOutlet weak var requestStatus: DesignableUILabel!
    
    @IBOutlet weak var distance: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    @IBOutlet weak var deliveryDate: DesignableUILabel!
    
    @IBOutlet weak var acceptButton: CustomButton!
    @IBOutlet weak var rejectButton: CustomButton!
    
    
    var orderDetail = OrderResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
    }
    
    func initializeView(){
        self.orderId.text = "Order ID - \(self.orderDetail.order_id ?? 0)"
        if(self.orderDetail.orderItems.count > 0){
          self.distance.text = "Distance: \(self.orderDetail.orderItems[0].distance ?? "") kms"
           self.address.text = self.orderDetail.orderItems[0].business?.location
        }
        self.deliveryDate.text = "\((self.orderDetail.created_at ?? "").prefix(10))"
        switch self.orderDetail.order_status {
        case "0":
            self.requestStatus.text = "Status: Pending"
            self.rejectButton.isHidden = false
            self.acceptButton.setTitle("Accept order", for: .normal)
            break
        case "1":
            self.requestStatus.text = "Status: Accepted"
            self.rejectButton.isHidden = true
            self.acceptButton.setTitle("Order accepted", for: .normal)
            break
        case "2":
            self.requestStatus.text = "Status: Rejected"
            self.rejectButton.isHidden = true
            self.acceptButton.setTitle("Order is rejected", for: .normal)
            break
        case "3":
            self.requestStatus.text = "Status: Preparing"
            self.rejectButton.isHidden = true
            self.acceptButton.setTitle("Order is making", for: .normal)
            break
        case "4":
            self.requestStatus.text = "Status: Out for delivery"
            self.rejectButton.isHidden = true
            self.acceptButton.setTitle("Out for delivery", for: .normal)
            break
        case "5":
            self.requestStatus.text = "Status: Delivered"
            self.rejectButton.isHidden = true
            self.acceptButton.setTitle("Order delivered", for: .normal)
            break
        default:
            break
        }
    }
    
    func changeOrderStatus(status: String){
        ActivityIndicator.show(view: self.view)
        let param:[String:Any]=[
            "order_status":status
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_ORDER_STATUS  + (self.orderDetail.id ?? ""), method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_ORDER_STATUS) { response in
            self.orderDetail.order_status = status
            self.initializeView()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func acceptAction(_ sender: UIButton) {
        switch sender.titleLabel?.text {
        case "Accept order":
            self.changeOrderStatus(status: "1")
            break
        case "Order accepted":
            self.changeOrderStatus(status: "3")
            break
        case "Order is making":
            self.changeOrderStatus(status: "4")
            break
        case "Out for delivery":
            self.changeOrderStatus(status: "5")
            break
        case "Order delivered":
            break
        default:
            break
        }
    }
    
    @IBAction func rejectAction(_ sender: UIButton) {
        self.changeOrderStatus(status: "2")
    }
    
}
