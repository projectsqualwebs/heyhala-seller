//
//  AccountSettingViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 27/04/21.
//

import UIKit

class AccountSettingViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var newPassword: DesignableUITextField!
    
    @IBOutlet weak var confirmPassword: DesignableUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newPassword.attributedPlaceholder = NSAttributedString(string: "New Password",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        confirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
