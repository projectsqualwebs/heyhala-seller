//
//  ProfileViewController.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//


import UIKit
import RAMAnimatedTabBarController

class ProfileViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var profileTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var fullName: DesignableUILabel!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var userEmail: DesignableUILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTab.animation.iconSelectedColor = .white
        self.profileTab.animation.textSelectedColor = .white
        profileTab.playAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fullName.text = Singleton.shared.userDetail.fullName
        self.userEmail.text = Singleton.shared.userDetail.email
        self.userImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (Singleton.shared.userDetail.profile ?? "")))
    }
    
    //MARK: IBAction
    @IBAction func profileAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func serviceAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 1
    }
    
    
    @IBAction func productAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    
    @IBAction func orderAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    
    @IBAction func paymentInfoAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentInfoViewController") as! PaymentInfoViewController
       // myVC.showBackButton = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func accountSettingAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingViewController") as! AccountSettingViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addStoreAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
        myVC.screenType = 3
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        UserDefaults.standard.removeObject(forKey: UD_USER_DETAIl)
        Singleton.shared.initialiseValues()
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func eventRequestAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentInfoViewController") as! PaymentInfoViewController
        myVC.isEventRequest = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}
