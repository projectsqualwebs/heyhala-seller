//
//  TabBarViewController.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//

import UIKit
import RAMAnimatedTabBarController


class TabbarViewController: RAMAnimatedTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 0
        self.selectedIndex = 2
        self.tabBar.tintColor = .white
       // self.tabBar.standardAppearance.selectionIndicatorTintColor = .white
        self.tabBar.unselectedItemTintColor = .white
        self.tabBar.unselectedItemTintColor = primaryColor
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .selected)
        NavigationController.shared.getAllStores { (response) in
            
        }
        NavigationController.shared.getAllCategories { (response) in
            
        }
    }
}
