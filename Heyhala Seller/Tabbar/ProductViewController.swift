//
//  ProductViewController.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//

import UIKit
import RAMAnimatedTabBarController


class ProductViewController: UIViewController, NewEntry {
    func newEntry() {
        Singleton.shared.allProduct = []
        NavigationController.shared.getAllProducts(view: self) { (response) in
            self.productData = response
            self.productTable.reloadData()
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var productTable: ContentSizedTableView!
    @IBOutlet weak var productTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var noProductLabel: DesignableUILabel!
    
    var productData = [GetStoreResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productTable.layer.cornerRadius = 10
        self.productTab.textColor = .white
        self.productTab.animation.iconSelectedColor = .white
        self.productTab.animation.textSelectedColor = .white
        self.productTab.playAnimation()
        NavigationController.shared.getAllProducts(view: self) { (response) in
            self.productData = response
            self.productTable.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("response")
    }
    
    
    //MARK: IBActions
    @IBAction func addAction(_ sender: Any) {
        if(Singleton.shared.storeData.count == 0){
            self.showMsg(controller: self, text: "Add store before adding product")
        }else {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
        myVC.screenType = 1
        myVC.newEntryDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
        }
    }

}

extension ProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.productData.count == 0){
            self.noProductLabel.isHidden = false
        }else {
            self.noProductLabel.isHidden = true
        }
        return self.productData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableCell") as! ProductTableCell
        let val = self.productData[indexPath.row]
        cell.productName.text = val.name
        cell.productImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (val.image ?? "")))
        cell.productPrice.text = "SAR \(val.price ?? 0)"
        
        cell.optionButton={
            var alert = UIAlertController(title: "Choose Option", message: nil, preferredStyle: .actionSheet)
            let alertOne = UIAlertAction(title: "Edit", style: .default) { (action) in
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
                myVC.screenType = 2
                myVC.editProduct = val
                myVC.newEntryDelegate = self
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            
            
            let alertTwo = UIAlertAction(title: "Delete", style: .default) { (action) in
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_REMOVE_PRODUCT + "\(val.id ?? "")", method: .delete, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_PRODUCT) { (response) in
                    self.showMsg(controller: self, text: "Product deleted successfully")
                    ActivityIndicator.hide()
                    self.newEntry()
                }
            }
            let alertThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(alertOne)
            alert.addAction(alertTwo)
            alert.addAction(alertThree)
            self.addActionSheetForiPad(actionSheet: alert)
            self.present(alert, animated: true, completion: nil)
        }
        return cell
    }
    
}

class ProductTableCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK: IBOutlets
    @IBOutlet weak var productImage: ImageView!
    @IBOutlet weak var productName: DesignableUILabel!
    @IBOutlet weak var productPrice: DesignableUILabel!
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var date: DesignableUILabel!
    @IBOutlet weak var orderStatus: DesignableUILabel!
    
    
    var optionButton:(()-> Void)? = nil
    var imageData = [ImagesResponse]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if(self.imageCollection != nil){
          imageCollection.delegate = self
          imageCollection.dataSource = self
        }
    }
    
    //MARK: IBActions
    @IBAction func optionAction(_ sender: Any) {
        if let optionButton = self.optionButton {
            optionButton()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollection", for: indexPath) as! ImageCollection
        cell.imageView.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.imageData[indexPath.row].image ?? "")))
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/3)-10, height: (collectionView.frame.width/3)-10)
    }
    
}

class ImageCollection: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var imageView: ImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemView: View!
    
    var removeButton:(()-> Void)? = nil
    
    //MARK: IBActions
    
    @IBAction func removeAction(_ sender: Any) {
        if let removeButton = self.removeButton{
            removeButton()
        }
    }
}
