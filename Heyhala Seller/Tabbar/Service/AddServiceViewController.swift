//
//  AddServiceViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 24/04/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import PINRemoteImage

protocol NewService {
    func newEntry()
}

class AddServiceViewController: UIViewController, SelectFromPicker {
    func selectedItem(name: String, index: Int) {
        self.selectedCategory = self.categoryData[index]
        self.category.text = name
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var editLabel: DesignableUILabel!
    @IBOutlet weak var categoryView: View!
    @IBOutlet weak var descriptionLabel: DesignableUILabel!
    @IBOutlet weak var logoImage: ImageView!
    @IBOutlet weak var saveButtonLabel: DesignableUILabel!
    @IBOutlet weak var licenseImage: ImageView!
    @IBOutlet weak var productName: DesignableUITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var category: DesignableUILabel!
    @IBOutlet weak var location: DesignableUILabel!
    @IBOutlet weak var priceView: View!
    @IBOutlet weak var priceField: DesignableUITextField!
    @IBOutlet weak var imageCollection: UICollectionView!
    
    var newEntryDelegate: NewService? = nil
    var categoryData = [GetCategoryResponse]()
    var imagePath  = ""
    var imagePath2  = [String]()    
    var imageName = ""
    var currentPick = 1
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    let picker = UIImagePickerController()
    var selectedCategory = GetCategoryResponse()
    var navigationFromVerification = false
    var editService = GetStoreResponse()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.delegate = self
        self.picker.delegate = self
        NavigationController.shared.getAllCategories { (response) in
            self.categoryData = response
        }
        
        productName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        self.initializeView()
    }
    
    func initializeView(){
        if(self.editService.id != nil){
            self.productName.text = editService.name
            self.textView.text = editService.description
            self.logoImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (editService.logo ?? "")))
            self.location.text = editService.location ?? ""
            self.imagePath = editService.logo   ?? ""
            if((editService.images?.count ?? 0) > 0){
                for val in editService.images! {
                    self.imagePath2.append(val.image ?? "")
                }
                
            }
            self.latitude =  CLLocationDegrees(editService.latitude ?? "0")!
            self.longitude = CLLocationDegrees(editService.longitude ?? "0")!
            
            let category = self.categoryData.filter{
                $0.id == editService.category
            }
            if(category.count > 0){
                self.selectedCategory = category[0]
            }
        }
    }
    
    
    //MARK: IBActions
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addLogoAction(_ sender: UIButton) {
        self.currentPick = sender.tag
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.addActionSheetForiPad(actionSheet: alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func categoryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        for val in categoryData {
            if(val.category_name == "EVENTS"){
                myVC.pickerData.append(val.category_name ?? "")
            }
        }
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func locationAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                    UInt(GMSPlaceField.placeID.rawValue) |
                                                    UInt(GMSPlaceField.coordinate.rawValue) |
                                                    GMSPlaceField.addressComponents.rawValue |
                                                    GMSPlaceField.formattedAddress.rawValue)
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func addServiceAction(_ sender: Any) {
        if(self.imagePath.isEmpty){
            self.showMsg(controller: self, text: "Pick Logo image")
        }else if(self.productName.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter name")
        }else if(selectedCategory.id == nil){
            self.showMsg(controller: self, text: "Select category")
        }else if(self.textView.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter description")
        }else if(self.location.text!.isEmpty){
            self.showMsg(controller: self, text: "Enter address")
        }else if(self.imagePath2.count == 0){
            self.showMsg(controller: self, text: "Pick event images")
        }else {
            var url = String()
            let param:[String:Any] =
                [
                    "name": self.productName.text ?? "",
                    "description": self.textView.text ?? "",
                    "category": self.selectedCategory.id ?? "",
                    "location": self.location.text ?? "",
                    "logo": self.imagePath,
                    "images":self.imagePath2,
                    "latitude": self.latitude,
                    "longitude": self.longitude,
                ]
            if(self.editService.id == nil){
                url = U_BASE + U_ADD_NEW_SERVICE
            }else {
                url = U_BASE + U_UPDATE_SERVICE + "\(self.editService.id ?? "")"
            }
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: url) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.serviceData = []
                if(self.editService.id == nil){
                    self.showMsg(controller: self, text: "Serice saved successfully")
                    self.newEntryDelegate?.newEntry()
                }else{
                    self.showMsg(controller: self, text: "Serice updated successfully")
                    self.newEntryDelegate?.newEntry()
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}


extension AddServiceViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "image": data!,
                "type": 6
            ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, objectClass: UploadImage.self, fileName: imageName) { (response) in
                
                if(self.currentPick == 1){
                    self.logoImage.image = cropImage
                    self.imagePath = response.response?.url ?? ""
                }else {
                    self.imagePath2.append(response.response?.url ?? "")
                    self.imageCollection.reloadData()
                }
                
                ActivityIndicator.hide()
                
            }
            
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
}



extension AddServiceViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.location.text = place.formattedAddress
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension AddServiceViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Description"){
            self.textView.text = ""
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagePath2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollection", for: indexPath) as! ImageCollection
        cell.imageView.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.imagePath2[indexPath.row] ?? "")))
        cell.removeButton={
            self.imagePath2.remove(at: indexPath.row)
            self.imageCollection.reloadData()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/3)-10, height: (collectionView.frame.width/3)-10)
    }
}
