//
//  EditServiceListViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 26/04/21.
//

import UIKit

class EditServiceListViewController: UIViewController {
   //MARK: IBOutlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBActions
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
