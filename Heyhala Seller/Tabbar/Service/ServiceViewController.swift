//
//  ServiceViewController.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//

import UIKit
import RAMAnimatedTabBarController

class ServiceViewController: UIViewController, NewService {
    func newEntry() {
        Singleton.shared.serviceData = []
        NavigationController.shared.getAllServices(view: self) { (response) in
            self.serviceData = response
            self.serviceTable.reloadData()
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var serviceTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var serviceTable: UITableView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    
    var serviceData = [GetStoreResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.serviceTable.estimatedRowHeight = 140
        self.serviceTable.rowHeight = UITableView.automaticDimension
        self.serviceTab.animation.iconSelectedColor = .white
        self.serviceTab.animation.textSelectedColor = .white
        serviceTab.playAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getAllServices(view: self) { (response) in
            self.serviceData = response
            self.serviceTable.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func addAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddServiceViewController") as! AddServiceViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func editAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EditServiceListViewController") as! EditServiceListViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension ServiceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.serviceData.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = true
        }
        return self.serviceData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableCell") as! ProductTableCell
        let val = self.serviceData[indexPath.row]
        cell.productName.text = val.name
        cell.productImage.pin_setImage(from: URL(string: U_IMAGE_BASE + (val.logo ?? "")))
        cell.productPrice.text = val.description
        if((val.images?.count ?? 0) > 0){
            cell.imageData = val.images!
            if(((cell.imageData.count/3)*80) == 0){
                cell.collectionHeight.constant = CGFloat(80) + 10
            }else{
                cell.collectionHeight.constant = CGFloat((val.images!.count/3)*80) + 10
            }
            cell.imageCollection.reloadData()
        }
        cell.optionButton={
            var alert = UIAlertController(title: "Choose Option", message: nil, preferredStyle: .actionSheet)
            let alertOne = UIAlertAction(title: "Edit", style: .default) { (action) in
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddServiceViewController") as! AddServiceViewController
                myVC.editService = val
                myVC.newEntryDelegate = self
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            
            let alertTwo = UIAlertAction(title: "Delete", style: .default) { (action) in
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_SERVICE + "\(val.id ?? "")", method: .delete, parameter: nil, objectClass: SuccessResponse.self, requestCode: U_REMOVE_PRODUCT) { (response) in
                    self.showMsg(controller: self, text: "Service deleted successfully")
                    ActivityIndicator.hide()
                    self.newEntry()
                }
            }
            let alertThree = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(alertThree)
            alert.addAction(alertOne)
            alert.addAction(alertTwo)
            self.addActionSheetForiPad(actionSheet: alert)
            self.present(alert, animated: true, completion: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceListDetailViewController") as! ServiceListDetailViewController
        myVC.serviceDetail = self.serviceData[indexPath.row]
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

