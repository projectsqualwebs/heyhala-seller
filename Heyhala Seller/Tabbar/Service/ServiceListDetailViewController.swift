//
//  ServiceListDetailViewController.swift
//  Heyhala Seller
//
//  Created by Sagar Pandit on 26/04/21.
//

import UIKit
import PINRemoteImage

class ServiceListDetailViewController: UIViewController, NewService {
    func newEntry() {
        Singleton.shared.serviceData = []
        self.currentIndex = 0
        NavigationController.shared.getAllServices(view: self) { (response) in
            let service =  response.filter{
                $0.id == self.serviceDetail.id
            }
            if(service.count > 0){
                self.serviceDetail = service[0]
                self.initializeView()
            }
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var eventImage: ImageView!
    @IBOutlet weak var eventName: DesignableUILabel!
    @IBOutlet weak var eventPrice: DesignableUILabel!
    @IBOutlet weak var eventDescription: DesignableUILabel!
    
    var currentIndex = 0
    var serviceDetail = GetStoreResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
    }
     
    func initializeView(){
        if((serviceDetail.images?.count ?? 0) > currentIndex){
            let image = U_IMAGE_BASE + (self.serviceDetail.images?[self.currentIndex].image ?? "")
            self.eventImage.pin_setImage(from: URL(string: image))
            
        }
        self.eventName.text = self.serviceDetail.name
        self.eventPrice.text = "SAR \(self.serviceDetail.price ?? 0).00"
        self.eventDescription.text = self.serviceDetail.description
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddServiceViewController") as! AddServiceViewController
        myVC.isEditing = true
        myVC.editService = self.serviceDetail
        myVC.newEntryDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func changeIndexAction(_ sender: UIButton) {
        if(sender.tag == 1){
            if(self.currentIndex > 0){
                let image = U_IMAGE_BASE + (self.serviceDetail.images?[self.currentIndex-1].image ?? "")
                self.eventImage.pin_setImage(from: URL(string: image))
                self.currentIndex -= 1
            }
            
        }else {
            if((self.serviceDetail.images?.count ?? 0) > (self.currentIndex+1)){
                let image = U_IMAGE_BASE + (self.serviceDetail.images?[self.currentIndex+1].image ?? "")
                self.eventImage.pin_setImage(from: URL(string: image))
                self.currentIndex += 1
            }
        }
    }
    
}
