//
//  Model.swift
//  Hey Hala
//
//  Created by qw on 09/01/21.
//

import Foundation

struct ErrorResponse: Codable{
    var message: String?
    var status: Int?
}

struct SuccessResponse: Codable{
    var message: String?
    var status: Int?
}

struct GetOtp: Codable {
    var response: GetOtpResponse?
    var message: String?
    var status: Int?
}

struct GetOtpResponse: Codable {
    var phone: String?
    var otp: Int?
    var id: Int?
    
}

struct Login: Codable {
    var response = LoginResponse()
    var message: String?
    var status: Int?
}

struct LoginResponse: Codable {
    var id: String?
    var fullName: String?
    var dob: String?
    var gender: String?
    var phone: String?
    var email: String?
    var token: String?
    var profile: String?
    var userType: String?
}

struct ChatDetail: Codable{
    var sender_id: Int?
    var receiver_id: Int?
    var receiver_name: String?
    var sender_name: String?
    var sender_image: String?
    var receiver_image: String?
    var shipment_id: Int?
}

struct ChatResponse: Codable{
    var sender_id: Int?
    var receiver_id: Int?
    var receiver_name: String?
    var sender_name: String?
    var sender_image: String?
    var receiver_image: String?
    var shipment_id: Int?
}

struct UploadImage: Codable{
    var response: UploadImageResponse?
    var message: String?
    var status: Int?
}

struct UploadImageResponse: Codable{
    var url: String?
}

struct GetCategories: Codable {
    var response = [GetCategoryResponse]()
    var message: String?
    var status: Int?
}

struct GetCategoryResponse: Codable {
    var id: String?
    var category_name: String?
    var category_type: String?
    var image: String?
}

struct GetStores: Codable {
    var response = [GetStoreResponse]()
    var message: String?
    var status: Int?
}

struct GetStoreResponse: Codable {
    var id: String?
    var business: String?
    var price: Double?
    var image: String?
    var user: String?
    var name: String?
    var description: String?
    var category: String?
    var location :String?
    var logo: String?
    var images:[ImagesResponse]?
    var license: String?
    var latitude: String?
    var longitude: String?
    var keywords: String?
    var ingredients: String?
    var preprationTime:Int?
    var preperationStatus:Int?
    var distance: String?
    var review : Review?
}

struct ImagesResponse: Codable {
    var id: Int?
    var image: String?
    var service: String?
}

struct GetOrders: Codable {
    var response = [OrderResponse]()
    var message: String?
    var status: Int?
}

struct OrderResponse: Codable {
    var id: String?
    var user: String?
    var address: String?
    var total: Double?
    var business: String?
    var order_id: Int?
    var delivery_type: Int?
    var created_at: String?
    var order_status: String?
    
    var orderItems = [OrderResponseDetail]()
}

struct OrderResponseDetail: Codable{
    var id: String?
    var order: String?
    var product: GetStoreResponse?
    var business: GetStoreResponse?
    var quantity: Int?
    var price: Double?
    var review: Review?
    var distance: String?
}


struct Review: Codable {
    var reviews: Int?
    var rating: Double?
}

struct GetServiceRequest: Codable {
    var response = [ServiceRequestResponse]()
    var message: String?
    var status: Int?
}

struct ServiceRequestResponse: Codable {
    var id: String?
    var user: String?
    var service: GetStoreResponse?
    var location: String?
    var description: String?
    var longitude: String?
    var latitude: String?
    var startTime: Int?
    var endTime: Int?
    var date: Int?
    var status:OrderStatus?
    var amount:Double?
}

struct OrderStatus: Codable {
    var id: Int?
    var serviceRequest: String?
    var price: Double?
    var status: String?
    var user: String?
}

struct GetDashboardData: Codable{
    var response = DashboardResponse()
    var message: String?
    var status: Int?
}

struct DashboardResponse: Codable {
    var id: String?
    var user: String?
    var name: String?
    var description: String?
    var location: String?
    var longitude: String?
    var latitude: String?
    var logo: String?
    var license: String?
    var status = DasboardStatus()
    
}

struct DasboardStatus: Codable {
    var newOrders: Int?
    var overallSales: Int?
    var returnOrders:Int?
    var completedOrders: Int?
    var orderInProgress: Int?
}
