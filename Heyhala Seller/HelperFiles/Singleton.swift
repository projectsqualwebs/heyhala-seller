//
//  Singleton.swift
//  FFK-KIOSK
//
//  Created by qw on 18/01/21.
//

import UIKit
import LocalAuthentication
import CoreLocation

class Singleton {
    static let shared = Singleton()
    var userDetail = getUserInfo()
    var allCategoryData = [GetCategoryResponse]()
    var storeData = [GetStoreResponse]()
    var allProduct = [GetStoreResponse]()
    var serviceData = [GetStoreResponse]()
    var dashboardData = DashboardResponse()
    
    public static func getUserInfo() -> LoginResponse{
        if let info = UserDefaults.standard.data(forKey: UD_USER_DETAIl){
            let userData = info
            let user = try! JSONDecoder().decode(LoginResponse.self, from: userData)
            return user
        }else {
            return LoginResponse()
        }
    }
    
    func saveUserInfo(user:LoginResponse){
        do{
            let data = try? JSONEncoder().encode(user)
            UserDefaults.standard.setValue(data, forKey: UD_USER_DETAIl)
        }catch{
            
        }
    }

    func initialiseValues(){
        userDetail = LoginResponse()
        storeData = []
        allProduct = []
        serviceData = []
     }
}

