//
//  Constants.swift
//  Hey Hala
//
//  Created by qw on 23/12/20.

import UIKit

let primaryColor = UIColor(red: 111/255, green: 1/255, blue: 1/255, alpha: 1)//#6F0101
let darkBrown = UIColor(red: 90/255, green: 2/255, blue: 2/255, alpha: 1) //#5A0202
let blackColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1) //#000000
let darkGrayColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 1) //#2C2C2C
let grayColor = UIColor(red: 58/255, green: 56/255, blue: 56/255, alpha: 1) //#3A3838

let U_BASE = "http://18.116.162.191:3333/"
let U_IMAGE_BASE =  "http://18.116.162.191:3333"
let U_UPLOAD_IMAGE = "file"

let U_LOGIN = "auth/login"
let U_SIGN_UP = "seller/auth/signup"
let U_UPDATE_USER = "user-update"
let U_GET_OTP = "otp/"
let U_VERIFY_OTP = "otp"
let U_GET_CATEGORIES = "categories"

let U_CREATE_BUSINESS = "business/"
let U_GET_BUSINESS = "businesses-user"
let U_DELETE_BUSINESS = "business/"
let U_GET_BUSINESS_BY_CATEGORY = "businesses/"
let U_BUSINESS_UPDATE = "business-update/"


let U_GET_PRODUCTS = "product-user"
let U_ADD_PRODUCT = "product"
let U_PRODUCT_BY_STORE = "products/"
let U_UPDATE_PRODUCT = "product-update/"
let U_REMOVE_PRODUCT = "product/"

let U_ADD_NEW_SERVICE = "service"
let U_GET_ALL_SERVICE = "services-user"
let U_GET_SERVICE_REQUEST = "service-requests"
let U_DELETE_SERVICE = "service/"
let U_UPDATE_SERVICE = "service-update/"
let U_UPDATE_SERVICE_REQUEST = "service-request-price/"


let U_GET_DASHBOARD_DATA = "dashboard-data/"

let U_GET_ORDERS = "orders-business/" //1:Accept,2:Reject, 3:Order preparing, 4:Out of delivery, 5: delivered
let U_UPDATE_ORDER_STATUS = "order-status/"

//User Defaults
let UD_USER_DETAIl = "userDetail"
let UD_TOKEN = "user_token"

//Notifications

let N_SHOW_ERROR_MESSAGE = "N_SHOW_ERROR_MESSAGE"
