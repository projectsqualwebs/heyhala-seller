//
//  NavigationController.swift
//  Hey Hala
//
//  Created by qw on 01/01/21.
//

import UIKit
import CoreLocation


class NavigationController: UIViewController, CLLocationManagerDelegate {
    //MARK: IBOutlets
    
    
    static let shared = NavigationController()
    lazy var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    

    func getAllCategories(completionHandler: @escaping ([GetCategoryResponse]) -> Void){
        if(Singleton.shared.allCategoryData.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CATEGORIES, method: .get, parameter: nil, objectClass: GetCategories.self, requestCode: U_GET_CATEGORIES) { (response) in
                Singleton.shared.allCategoryData = response.response
                completionHandler(Singleton.shared.allCategoryData)
            }
        }else {
            completionHandler(Singleton.shared.allCategoryData)
        }
    }
    
    func getAllStores(completionHandler: @escaping ([GetStoreResponse]) -> Void){
        if(Singleton.shared.storeData.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_BUSINESS, method: .get, parameter: nil, objectClass: GetStores.self, requestCode: U_GET_BUSINESS) { (response) in
                Singleton.shared.storeData = response.response
                completionHandler(Singleton.shared.storeData)
            }
        }else {
            completionHandler(Singleton.shared.storeData)
        }
    }
    
    func getAllProducts(view: UIViewController,completionHandler: @escaping ([GetStoreResponse]) -> Void){
        if(Singleton.shared.allProduct.count == 0){
            ActivityIndicator.show(view: view.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PRODUCTS, method: .get, parameter: nil, objectClass: GetStores.self, requestCode: U_GET_PRODUCTS) { (response) in
                Singleton.shared.allProduct = response.response
                completionHandler(Singleton.shared.allProduct)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.allProduct)
        }
    }
    
    func getAllServices(view: UIViewController,completionHandler: @escaping ([GetStoreResponse]) -> Void){
        if(Singleton.shared.serviceData.count == 0){
            ActivityIndicator.show(view: view.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ALL_SERVICE, method: .get, parameter: nil, objectClass: GetStores.self, requestCode: U_GET_ALL_SERVICE) { (response) in
                Singleton.shared.serviceData = response.response
                completionHandler(Singleton.shared.serviceData)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.serviceData)
        }
    }

    
    func getDashboardData(view: UIViewController,completionHandler: @escaping (DashboardResponse) -> Void){
        if(Singleton.shared.serviceData.count == 0){
            ActivityIndicator.show(view: view.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DASHBOARD_DATA + "2021", method: .get, parameter: nil, objectClass: GetDashboardData.self, requestCode: U_GET_DASHBOARD_DATA) { (response) in
                Singleton.shared.dashboardData = response.response
                completionHandler(Singleton.shared.dashboardData)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.dashboardData)
        }
    }
}




