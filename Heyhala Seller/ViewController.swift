//
//  ViewController.swift
//  Heyhala Seller
//
//  Created by Apple on 20/04/21.
//

import UIKit

class ViewController: UIViewController {
    //MARK: IBOutlets
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
            let myVC  = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
        let myVC  = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.showErrorMessage(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
    }
    
    @objc func showErrorMessage(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        if let val = notif.userInfo?["msg"] as? String{
            self.showMsg(controller: self, text: val)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.showErrorMessage(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
    }
}
